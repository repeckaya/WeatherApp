import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import weatherReducer from "./weatherReducer/weatherReducer";

let reducers = combineReducers({
    weatherData: weatherReducer,
})

let store = createStore(reducers, applyMiddleware(thunkMiddleware));

export default store;

