import { LOCALES } from './locales'

export const messages = {
    [LOCALES.ENGLISH]: {
        humidity: `Humidity:`,
        pressure: `Pressure:`,
        wind_speed: `Wind speed:`,
        string: `Right now in`,
        forecast: `Show me the forecast`,
        back: `Home page`,
        error: `City not found`,
    },
    [LOCALES.RUSSIAN]: {
        humidity: 'Влажность:',
        pressure: 'Давление:',
        wind_speed: 'Скорость ветра:',
        string: `Сейчас в`,
        forecast: `Показать прогноз погоды`,
        back: `Главная страница`,
        error: `Город не найден`,
    },
}