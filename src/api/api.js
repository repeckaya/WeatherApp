import * as axios from "axios";

const instance = axios.create({
    baseURL: 'http://api.weatherapi.com/v1',
    headers: {
        'Access-Control-Allow-Origin': '*',
    }
})

export const weatherAPI = {
    getCurrentWeather(city, language) {
        return instance.get(`/forecast.json?key=${process.env.REACT_APP_API_WEATHER}&q=${city}&days=3&aqi=no&alerts=no&lang=${language}`)
    }
}





