import React from "react";
import Search from "./Search";
import styles from '../assets/styles/WeatherData.module.scss'
import {connect} from "react-redux";
import {FormattedMessage} from 'react-intl';
import {getWeather} from "../store/weatherReducer/weatherReducer";
import {NavLink} from "react-router-dom";
import ErrorPage from "./ErrorPage";

const WeatherData = (props) => {
    const pressureToMmOfMercury = (props.pressure * 0.750063755419211).toFixed();
    const windToMPS = (props.wind * 1000 / 3600).toFixed(1);
    const condition = props.condition.charAt(0).toUpperCase() + props.condition.slice(1);
    if (props.error === 1006) {
        return  <ErrorPage location={props.location} getWeather={props.getWeather}
                           responseLocation={props.responseLocation}  handleChange={props.handleChange} />
    }
        return (
            <div className={styles.wrapper}>
                <div className={styles.container}>
                    <Search location={props.location} getWeather={props.getWeather}
                            responseLocation={props.responseLocation}  handleChange={props.handleChange} />
                    <div className={styles.row}>
                        <div className={styles.left}>
                            <div className={styles.high}>
                                <p><FormattedMessage id='string'/><span> {props.responseLocation}, {props.country}</span>
                                </p>
                                <p>{condition}</p>
                            </div>
                            <div className={styles.low}>
                                <p><FormattedMessage id='pressure'/> {pressureToMmOfMercury}<span>mm Hg</span></p>
                                <p><FormattedMessage id='humidity'/> {props.humidity}<span>%</span></p>
                                <p><FormattedMessage id='wind_speed'/> {windToMPS}<span>m/s</span></p>
                            </div>
                        </div>
                        <div className={styles.right}>
                            <p>{props.temp}&deg;C</p>
                            <img src={props.icon} alt=''/>
                        </div>
                    </div>
                </div>
                <NavLink to='/forecast' className={styles.forecastButton}>
                    <p><FormattedMessage id='forecast'/></p>
                </NavLink>
            </div>
        )
}

const mapStateToProps = (state) => ({
    location: state.weatherData.location,
    responseLocation: state.weatherData.responseLocation,
    country: state.weatherData.country,
    temp: state.weatherData.temp_c,
    wind: state.weatherData.wind,
    icon: state.weatherData.icon,
    pressure: state.weatherData.pressure,
    humidity: state.weatherData.humidity,
    condition: state.weatherData.condition,
    error: state.weatherData.error,
})

export default connect(mapStateToProps, {getWeather})(WeatherData);