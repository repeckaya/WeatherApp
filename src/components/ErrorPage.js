import React, {useEffect} from "react";
import {FormattedMessage} from "react-intl";
import styles from "../assets/styles/WeatherData.module.scss";
import Search from "./Search";

const ErrorPage = (props) => {
    return (
        <div className={styles.wrapper}>
            <Search location={props.location} getWeather={props.getWeather}
                    responseLocation={props.responseLocation}  handleChange={props.handleChange} />
            <p className={styles.error}><FormattedMessage id='error'/></p>
        </div>
    )
}

export default ErrorPage;